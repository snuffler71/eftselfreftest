﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EFCodeFirstTest;

namespace EFMVC.Controllers
{
    public class PensionersController : Controller
    {
        private TestContext db = new TestContext();

        // GET: Pensioners
        public ActionResult Index()
        {
            return View(db.Pensioners.ToList());
        }

        // GET: Pensioners/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pensioner pensioner = db.Pensioners.Find(id);
            if (pensioner == null)
            {
                return HttpNotFound();
            }
            return View(pensioner);
        }

        // GET: Pensioners/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pensioners/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Type,RecordState,Data,RecordType,InpsCode,IbanType,PensionerLastName,PensionerFirstName,DelegateLastName,PaymentMethod,FundingCurrency,PensionerDob")] Pensioner pensioner)
        {
            if (ModelState.IsValid)
            {
                db.Pensioners.Add(pensioner);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pensioner);
        }

        // GET: Pensioners/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pensioner pensioner = db.Pensioners.Find(id);
            if (pensioner == null)
            {
                return HttpNotFound();
            }
            return View(pensioner);
        }

        // POST: Pensioners/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Type,RecordState,Data,RecordType,InpsCode,IbanType,PensionerLastName,PensionerFirstName,DelegateLastName,PaymentMethod,FundingCurrency,PensionerDob")] Pensioner pensioner)
        {
            if (ModelState.IsValid)
            {
                var pensionerService = new PensionerService();
                pensionerService.StartFlowOfWork(pensioner);
                return RedirectToAction("Index");
            }
            return View(pensioner);
        }

        // GET: Pensioners/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pensioner pensioner = db.Pensioners.Find(id);
            if (pensioner == null)
            {
                return HttpNotFound();
            }
            return View(pensioner);
        }

        // POST: Pensioners/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pensioner pensioner = db.Pensioners.Find(id);
            db.Pensioners.Remove(pensioner);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
