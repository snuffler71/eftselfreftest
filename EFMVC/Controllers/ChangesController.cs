﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EFCodeFirstTest;
using Newtonsoft.Json;

namespace EFMVC.Controllers
{
    public class ChangesController : Controller
    {
        private TestContext context = new TestContext();
        // GET: Changes
        public ActionResult Index(int id)
        {

            var workingCopies = context.ChangeTrackers.AsNoTracking().ToList();
            var converted = workingCopies.Select(wc => JsonConvert.DeserializeObject<WorkingCopy>(wc.Data));

            var changesToPenioner = converted
                .Where(c => c.EntityFrameworkEvent.Entries.Any(e => e.PrimaryKey.Id == id))
                .OrderByDescending(x => x.EndDate)
                .ToList();

            return View(changesToPenioner);
        }

        // GET: Changes/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Changes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Changes/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Changes/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Changes/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Changes/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Changes/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
