﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EFCodeFirstTest;

namespace EFMVC.Controllers
{
    public class PensionsController : Controller
    {
        private TestContext db = new TestContext();

        // GET: Pensions
        public ActionResult Index()
        {
            return View(db.Pensions.ToList());
        }

        // GET: Pensions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pension pension = db.Pensions.Find(id);
            if (pension == null)
            {
                return HttpNotFound();
            }
            return View(pension);
        }

        // GET: Pensions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Pensions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Data,RecordType,PensionAmount,Type,RecordState")] Pension pension)
        {
            if (ModelState.IsValid)
            {
                db.Pensions.Add(pension);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(pension);
        }

        // GET: Pensions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pension pension = db.Pensions.Find(id);
            if (pension == null)
            {
                return HttpNotFound();
            }
            return View(pension);
        }

        // POST: Pensions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Data,RecordType,PensionAmount,Type,RecordState")] Pension pension)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pension).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(pension);
        }

        // GET: Pensions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Pension pension = db.Pensions.Find(id);
            if (pension == null)
            {
                return HttpNotFound();
            }
            return View(pension);
        }

        // POST: Pensions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Pension pension = db.Pensions.Find(id);
            db.Pensions.Remove(pension);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
