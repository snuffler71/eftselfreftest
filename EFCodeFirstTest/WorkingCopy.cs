﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace EFCodeFirstTest
{
    public class WorkingCopy
    {
        public string EventType { get; set; }
        public Environment Environment { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int Duration { get; set; }
        public EntityFrameworkEvent EntityFrameworkEvent { get; set; }
    }

    public class Environment
    {
        public string UserName { get; set; }
        public string MachineName { get; set; }
        public string DomainName { get; set; }
        public string CallingMethodName { get; set; }
        public string AssemblyName { get; set; }
        public string Culture { get; set; }
    }

    public class PrimaryKey
    {
        public int Id { get; set; }
    }

    public class Change
    {
        public string ColumnName { get; set; }
        public string OriginalValue { get; set; }
        public string NewValue { get; set; }
    }

    public class ColumnValues
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class Entry
    {
        public string Table { get; set; }
        public string Action { get; set; }
        public PrimaryKey PrimaryKey { get; set; }
        public List<Change> Changes { get; set; }
        public ColumnValues ColumnValues { get; set; }
        public bool Valid { get; set; }
        public List<object> ValidationResults { get; set; }
    }

    public class EntityFrameworkEvent
    {
        public string Database { get; set; }
        public List<Entry> Entries { get; set; }
        public int Result { get; set; }
        public bool Success { get; set; }
    }


}
