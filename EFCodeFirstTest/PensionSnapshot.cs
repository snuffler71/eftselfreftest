﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Audit.EntityFramework;

namespace EFCodeFirstTest
{
    [AuditInclude]
    public class PensionSnapshot : BaseModel
    {
        public int PensionId { get; set; }

        [MaxLength(491)]
        [StringLength(491)]
        public string Data { get; set; } // Data (length: 491)

        [MaxLength(50)]
        [StringLength(50)]
        public string RecordType { get; set; } // Record Type (length: 50)

        [MaxLength(9)]
        [StringLength(9)]
        public string PensionAmount { get; set; } // Pension Amount (length: 9)
    }
}
