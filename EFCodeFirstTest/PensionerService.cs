﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace EFCodeFirstTest
{
    public class PensionerService
    {
        public void StartFlowOfWork(Pensioner pensioner)
        {
            using (var context = new TestContext()) 
            {
                if (pensioner.Id == 0)
                {
                    pensioner.Type = RecordType.GoldCopy;
                    pensioner.RecordState = RecordState.SanctionComplete;                  
                    context.Pensioners.Add(pensioner);
                    context.SaveChanges();
                }

                var currentPensioner = context.Pensioners.AsNoTracking().Single(p => p.Id == pensioner.Id);

                if (pensioner.Type == RecordType.GoldCopy)
                    pensioner.PensionerSnapshot = Mapper.Map<PensionerSnapshot>(currentPensioner);

                pensioner.Type = RecordType.WorkingCopy;
                pensioner.RecordState = RecordState.Validation;
                context.Entry(pensioner).State = EntityState.Modified;

                context.SaveChanges();

            }
        }
    }
}
