﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace EFCodeFirstTest
{
    public class AutoMapperConfigurator
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Pensioner, Pensioner>()
                .ForMember(e => e.PensionerSnapshot, opt => opt.Ignore())
                .ForMember(e => e.Pensions, opt => opt.Ignore());
                cfg.CreateMap<Pension, Pension>();
                cfg.CreateMap<Pension, PensionSnapshot>()
                    .ForMember(e => e.PensionId, opt => opt.MapFrom(m => m.Id))
                    .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
                cfg.CreateMap<Pensioner, PensionerSnapshot>()
                    .ForMember(e => e.PensionerId, opt => opt.MapFrom(m => m.Id))
                    .ForAllMembers(opts => opts.Condition((src, dest, srcMember) => srcMember != null));
            });

        }
    }
}
