namespace EFCodeFirstTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removePensionerId : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Pensioners", "PensionerId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pensioners", "PensionerId", c => c.Int(nullable: false));
        }
    }
}
