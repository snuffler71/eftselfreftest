namespace EFCodeFirstTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class snapshot : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PensionerSnapshots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PensionerId = c.Int(nullable: false),
                        Data = c.String(maxLength: 491),
                        RecordType = c.String(maxLength: 50),
                        InpsCode = c.String(maxLength: 5),
                        IbanType = c.String(maxLength: 1),
                        PensionerLastName = c.String(maxLength: 20),
                        PensionerFirstName = c.String(maxLength: 18),
                        DelegateLastName = c.String(maxLength: 20),
                        PaymentMethod = c.String(maxLength: 1),
                        FundingCurrency = c.String(maxLength: 1),
                        PensionerDob = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PensionSnapshots",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PensionId = c.Int(nullable: false),
                        Data = c.String(maxLength: 491),
                        RecordType = c.String(maxLength: 50),
                        PensionAmount = c.String(maxLength: 9),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Pensioners", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Pensioners", "RecordState", c => c.Int(nullable: false));
            AddColumn("dbo.Pensioners", "Data", c => c.String(maxLength: 491));
            AddColumn("dbo.Pensioners", "RecordType", c => c.String(maxLength: 50));
            AddColumn("dbo.Pensioners", "InpsCode", c => c.String(maxLength: 5));
            AddColumn("dbo.Pensioners", "IbanType", c => c.String(maxLength: 1));
            AddColumn("dbo.Pensioners", "PensionerLastName", c => c.String(maxLength: 20));
            AddColumn("dbo.Pensioners", "PensionerFirstName", c => c.String(maxLength: 18));
            AddColumn("dbo.Pensioners", "DelegateLastName", c => c.String(maxLength: 20));
            AddColumn("dbo.Pensioners", "PaymentMethod", c => c.String(maxLength: 1));
            AddColumn("dbo.Pensioners", "FundingCurrency", c => c.String(maxLength: 1));
            AddColumn("dbo.Pensioners", "PensionerDob", c => c.DateTime());
            AddColumn("dbo.Pensioners", "PensionerSnapshot_Id", c => c.Int());
            AddColumn("dbo.Pensions", "Data", c => c.String(maxLength: 491));
            AddColumn("dbo.Pensions", "RecordType", c => c.String(maxLength: 50));
            AddColumn("dbo.Pensions", "PensionAmount", c => c.String(maxLength: 9));
            AddColumn("dbo.Pensions", "Type", c => c.Int(nullable: false));
            AddColumn("dbo.Pensions", "RecordState", c => c.Int(nullable: false));
            AddColumn("dbo.Pensions", "PensionSnapshot_Id", c => c.Int());
            CreateIndex("dbo.Pensioners", "PensionerSnapshot_Id");
            CreateIndex("dbo.Pensions", "PensionSnapshot_Id");
            AddForeignKey("dbo.Pensioners", "PensionerSnapshot_Id", "dbo.PensionerSnapshots", "Id");
            AddForeignKey("dbo.Pensions", "PensionSnapshot_Id", "dbo.PensionSnapshots", "Id");
            DropColumn("dbo.Pensioners", "Name");
            DropColumn("dbo.Pensions", "Name");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Pensions", "Name", c => c.String());
            AddColumn("dbo.Pensioners", "Name", c => c.String());
            DropForeignKey("dbo.Pensions", "PensionSnapshot_Id", "dbo.PensionSnapshots");
            DropForeignKey("dbo.Pensioners", "PensionerSnapshot_Id", "dbo.PensionerSnapshots");
            DropIndex("dbo.Pensions", new[] { "PensionSnapshot_Id" });
            DropIndex("dbo.Pensioners", new[] { "PensionerSnapshot_Id" });
            DropColumn("dbo.Pensions", "PensionSnapshot_Id");
            DropColumn("dbo.Pensions", "RecordState");
            DropColumn("dbo.Pensions", "Type");
            DropColumn("dbo.Pensions", "PensionAmount");
            DropColumn("dbo.Pensions", "RecordType");
            DropColumn("dbo.Pensions", "Data");
            DropColumn("dbo.Pensioners", "PensionerSnapshot_Id");
            DropColumn("dbo.Pensioners", "PensionerDob");
            DropColumn("dbo.Pensioners", "FundingCurrency");
            DropColumn("dbo.Pensioners", "PaymentMethod");
            DropColumn("dbo.Pensioners", "DelegateLastName");
            DropColumn("dbo.Pensioners", "PensionerFirstName");
            DropColumn("dbo.Pensioners", "PensionerLastName");
            DropColumn("dbo.Pensioners", "IbanType");
            DropColumn("dbo.Pensioners", "InpsCode");
            DropColumn("dbo.Pensioners", "RecordType");
            DropColumn("dbo.Pensioners", "Data");
            DropColumn("dbo.Pensioners", "RecordState");
            DropColumn("dbo.Pensioners", "Type");
            DropTable("dbo.PensionSnapshots");
            DropTable("dbo.PensionerSnapshots");
        }
    }
}
