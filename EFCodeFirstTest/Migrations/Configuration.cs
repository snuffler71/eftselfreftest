using Audit.Core;

namespace EFCodeFirstTest.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Configuration;

    internal sealed class Configuration : DbMigrationsConfiguration<EFCodeFirstTest.TestContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;

            Audit.EntityFramework.Configuration.Setup()
                .ForContext<TestContext>(
                    config => config
                    .IncludeEntityObjects()
                    //.AuditEventType("{context}:{database}")
                    )
                .UseOptIn();

            Audit.Core.Configuration.Setup()
                .UseSqlServer(config => config
                    .ConnectionString(ConfigurationManager.ConnectionStrings["TestContext"].ConnectionString)
                    .Schema("dbo")
                    .TableName("ChangeTrackers")
                    .IdColumnName("ChangeTrackerId")
                    .JsonColumnName("Data")
                    .LastUpdatedColumnName("LastUpdatedDate"));
        }

        protected override void Seed(EFCodeFirstTest.TestContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
