namespace EFCodeFirstTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChangeTrackers",
                c => new
                    {
                        ChangeTrackerId = c.Int(nullable: false, identity: true),
                        InsertedDate = c.DateTime(nullable: false, defaultValueSql: "GetUTCDate()"),
                        LastUpdatedDate = c.DateTime(),
                        Data = c.String(),
                    })
                .PrimaryKey(t => t.ChangeTrackerId);
            
            CreateTable(
                "dbo.GeneralInspections",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MaxInspectionDate = c.DateTime(),
                        ScheduledDate = c.DateTime(),
                        InspectionDate = c.DateTime(),
                        PreviousInspectionDate = c.DateTime(),
                        Comment = c.String(),
                        PreviousInspectionID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.GeneralInspections", t => t.PreviousInspectionID)
                .Index(t => t.PreviousInspectionID);
            
            CreateTable(
                "dbo.Pensioners",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PensionerId = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pensions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PensionPensioners",
                c => new
                    {
                        Pension_Id = c.Int(nullable: false),
                        Pensioner_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Pension_Id, t.Pensioner_Id })
                .ForeignKey("dbo.Pensions", t => t.Pension_Id, cascadeDelete: true)
                .ForeignKey("dbo.Pensioners", t => t.Pensioner_Id, cascadeDelete: true)
                .Index(t => t.Pension_Id)
                .Index(t => t.Pensioner_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PensionPensioners", "Pensioner_Id", "dbo.Pensioners");
            DropForeignKey("dbo.PensionPensioners", "Pension_Id", "dbo.Pensions");
            DropForeignKey("dbo.GeneralInspections", "PreviousInspectionID", "dbo.GeneralInspections");
            DropIndex("dbo.PensionPensioners", new[] { "Pensioner_Id" });
            DropIndex("dbo.PensionPensioners", new[] { "Pension_Id" });
            DropIndex("dbo.GeneralInspections", new[] { "PreviousInspectionID" });
            DropTable("dbo.PensionPensioners");
            DropTable("dbo.Pensions");
            DropTable("dbo.Pensioners");
            DropTable("dbo.GeneralInspections");
            DropTable("dbo.ChangeTrackers");
        }
    }
}
