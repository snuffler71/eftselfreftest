﻿namespace EFCodeFirstTest
{
    public enum RecordState
    {
        SanctionComplete,
        Suspended,
        Validation,
        ValidationFailed,
        Enrichment,
        AddressScrubbing,
        AddressScrubbingFailed,
        BankScrubbing,
        BankScrubbingFailed,
        SanctionProcessing
    }
}