﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Audit.EntityFramework;
using System.ComponentModel.DataAnnotations;

namespace EFCodeFirstTest
{
    [AuditInclude]
    public class Pensioner: BaseModel
    {
        public virtual ICollection<Pension> Pensions { get; set; }
        //public virtual ICollection<ChangeTracker> Changes { get; set; }
        public RecordType Type { get; set; }
        public RecordState RecordState { get; set; }

        [MaxLength(491)]
        [StringLength(491)]
        public string Data { get; set; } // Data (length: 491)

        [MaxLength(50)]
        [StringLength(50)]
        public string RecordType { get; set; } // Record Type (length: 50)

        [MaxLength(5)]
        [StringLength(5)]
        public string InpsCode { get; set; } // INPS Code (length: 5)

        [MaxLength(1)]
        [StringLength(1)]
        public string IbanType { get; set; } // IBAN Type (length: 1)


        [MaxLength(20)]
        [StringLength(20)]
        public string PensionerLastName { get; set; } // Pensioner Last Name (length: 20)

        [MaxLength(18)]
        [StringLength(18)]
        public string PensionerFirstName { get; set; } // Pensioner First Name (length: 18)


        [MaxLength(20)]
        [StringLength(20)]
        public string DelegateLastName { get; set; } // Delegate Last Name (length: 20)

        [MaxLength(1)]
        [StringLength(1)]
        public string PaymentMethod { get; set; } // Payment Method (length: 1)

        [MaxLength(1)]
        [StringLength(1)]
        public string FundingCurrency { get; set; } // Funding Currency (length: 1)

        public System.DateTime? PensionerDob { get; set; } // Pensioner DOB

        public virtual PensionerSnapshot PensionerSnapshot { get; set; }
    }
}
