﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstTest
{
//CREATE TABLE[Event]
//(
//[EventId] BIGINT IDENTITY(1,1) NOT NULL,
//[InsertedDate] DATETIME NOT NULL DEFAULT(GETUTCDATE()),
//[LastUpdatedDate]
//DATETIME NULL,
//[Data] NVARCHAR(MAX) NOT NULL,
//CONSTRAINT PK_Event PRIMARY KEY(EventId)
//)
//GO
    public class ChangeTracker
    {
        [Key]
        public int ChangeTrackerId { get; set; }

        [Required, DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public DateTime InsertedDate { get; set; }

        public DateTime? LastUpdatedDate { get; set; }

        public string Data { get; set; }

    }
}
