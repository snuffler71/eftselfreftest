using System.Threading;
using Audit.EntityFramework;

namespace EFCodeFirstTest
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    //[AuditDbContext(Mode = AuditOptionMode.OptIn, IncludeEntityObjects = true, AuditEventType = "{database}_{context}")]
    public class TestContext : Audit.EntityFramework.AuditDbContext
    {
        // Your context has been configured to use a 'TestContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'EFCodeFirstTest.TestContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'TestContext' 
        // connection string in the application configuration file.
        public TestContext()
            : base("name=TestContext")
        {
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<GeneralInspection> GeneralInspections { get; set; }
        public DbSet<Pensioner> Pensioners { get; set; }
        public DbSet<ChangeTracker> ChangeTrackers { get; set; }

        public System.Data.Entity.DbSet<EFCodeFirstTest.Pension> Pensions { get; set; }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}