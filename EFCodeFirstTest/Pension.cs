﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Audit.EntityFramework;

namespace EFCodeFirstTest
{
    [AuditInclude]
    public class Pension: BaseModel
    {
        public virtual ICollection<Pensioner> Pensioners { get; set; }
        //public virtual ICollection<ChangeTracker> Changes { get; set; }

        [MaxLength(491)]
        [StringLength(491)]
        public string Data { get; set; }

        [MaxLength(50)]
        [StringLength(50)]
        public string RecordType { get; set; } 

        [MaxLength(9)]
        [StringLength(9)]
        public string PensionAmount { get; set; }

        public RecordType Type { get; set; }
        public RecordState RecordState { get; set; }


        public virtual PensionSnapshot PensionSnapshot { get; set; }
    }
}