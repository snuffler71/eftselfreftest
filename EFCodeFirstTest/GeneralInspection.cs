﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace EFCodeFirstTest
{
    public class GeneralInspection: BaseModel
    {
        [DataMember]
        public DateTime? MaxInspectionDate { get; set; }

        [DataMember]
        public DateTime? ScheduledDate { get; set; }


        [DataMember]
        public DateTime? InspectionDate { get; set; }

        [DataMember]
        public DateTime? PreviousInspectionDate { get; set; }

        [DataMember]
        public string Comment { get; set; }


        [DataMember]
        public int? PreviousInspectionID { get; set; }

        [DataMember]
        [ForeignKey("PreviousInspectionID")]
        public virtual GeneralInspection PreviousInspection { get; set; }

        public virtual List<GeneralInspection> Children { get; set; }
    }
}
