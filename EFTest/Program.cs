﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EFCodeFirstTest;
using Newtonsoft.Json;

namespace EFTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new TestContext();

            var p = new Pensioner
            {
                PensionerFirstName = "Graham",
                Pensions = new List<Pension>() { new Pension() { RecordType = "P1"} }
            };

            context.Pensioners.Add(p);
            context.SaveChanges();

            p.PensionerFirstName = "Victoria";

            context.SaveChanges();

            p.Pensions.First().RecordType = "P2";

            context.SaveChanges();

            var workingCopies = context.ChangeTrackers.AsNoTracking().ToList();
            
            var converted = workingCopies.Select(wc => JsonConvert.DeserializeObject<WorkingCopy>(wc.Data));

            Console.ReadLine();

            var changesToPenioner = converted
                .Where(c => 
                c.EntityFrameworkEvent.Entries
                .Any(e => e.PrimaryKey.Id == p.Id && e.Action == "Update"))
                .OrderByDescending(x => x.EndDate)
                .ToList();

        }
    }
}





//var i = new GeneralInspection()
//{
//PreviousInspection = new GeneralInspection()
//};

//var i2 = new GeneralInspection
//{
//Children = new List<GeneralInspection>() { new GeneralInspection() { Children = new List<GeneralInspection>() { new GeneralInspection() } }, new GeneralInspection() }
//};

//context.GeneralInspections.Add(i);
//context.GeneralInspections.Add(i2);

//context.SaveChanges();

//var inspections = context.GeneralInspections.Include("PreviousInspection").Include("Children").ToList();

